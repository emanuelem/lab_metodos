# Respostas dos Exercícios da aula do dia 09/11/2016

## Questão 1:
### a) Por sistema linear:

**Coeficientes do polinômio**: 

**f(2):**

### b) Pela forma de Lagrange:

**Coeficientes do polinômio**: 

**f(2):**

### c) Pela forma de Newton:

**Coeficientes do polinômio**: 

**f(2):**


## Questão 2

### Polinômio de grau 2
* Número de pontos e pontos utilizados (valores de x):
* Coeficientes do polinômio:
* f(3.4):

### Polinômio de grau 3
* Número de pontos e pontos utilizados (valores de x):
* Coeficientes do polinômio:
* f(3.4):

### Polinômio de grau 4
* Número de pontos e pontos utilizados (valores de x):
* Coeficientes do polinômio:
* f(3.4): 

## Questão 3

### a) Viscosidade para T=8ºC
* Interpolação Linear: Coeficientes do Polinômio e estimativa encontrada:
* Interpolação Quadrática: Coeficientes do Polinômio e estimativa encontrada:

### b) Temperatura para Viscosidade= 1.2 cP
* Coeficientes do polinômio encontrado:
* f(x) e aproximação inicial fornecidas para o método de Newton:
* Valor da temperatura: