# Respostas dos Exercícios da aula do dia 03/10/2016

## Q1: 2^x - 3x = 0
### Número de raízes:
R. 

### Subintervalos contendo as raízes:
R. 

### Resultados obtidos pelo Método da Bisseção (para cada subintervalo) com epsilon = 0.00001
 * Subintervalo: 
 
 * Raiz:

 * Número de iterações: 

### Resultados obtidos pelo Método da Posição Falsa (para cada subintervalo) epsilon = 0.00001
 * Subintervalo:
 
 * Raiz:

 * Número de iterações: 

**Para cada subintervalo, que método funcionou melhor? Por quê?**

## Q2

### 1. Resultado do Método de Newton:
Raiz e número de iterações:

### 2. Resultado do Método da Secante:
Raiz e número de iterações:
 

## Q3

Vetor solução: 

## Q4

Vetor solução:

Determinante:

# Q5

Vetor solução:
